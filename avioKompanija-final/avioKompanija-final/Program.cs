﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Net;


namespace avikompanija
{
    class Program
    {
        static void Main(string[] args)
        {
            //test();

            string connetionString = "Data Source=localhost;Initial Catalog=aviokompanija2;User ID=root;Password=";
            MySqlConnection conn = new MySqlConnection(connetionString);
            conn.Open();

            MySqlCommand command = conn.CreateCommand();
            //region kreiranje tabela
            //createTable(command);
            //endregion

            //region ubacivanje podataka u tabele
            //fillTable(command);
            //endregion

            selektovi(command);
            Console.WriteLine("Zavrsen unos");


            conn.Close();
            Console.ReadKey();
        }

        public static void createTable(MySqlCommand command)
        {

            #region putnici 

            command.CommandText = "CREATE TABLE IF NOT EXISTS putnici ("
                        + "idPutnika        INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                        + "ime              VARCHAR(255) NOT NULL,"
                        + "prezime          VARCHAR(255) NOT NULL,"
                        + "datumRodjenja    DATE NOT NULL,"
                        + "telefon          VARCHAR(255) NOT NULL,"
                        + "email            VARCHAR(255) NOT NULL,"
                        + "adresa           VARCHAR(255) NOT NULL,"
                        + "brojPasosa       INT NOT NULL,"
                        + "drzava           VARCHAR(255) NOT NULL)";
            executeNonQuery(command);
            #endregion

            #region zaposleni 

            command.CommandText = "CREATE TABLE IF NOT EXISTS zaposleni ("
                       + "idZaposleni        INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                       + "ime                VARCHAR(255) NOT NULL,"
                       + "prezime            VARCHAR(255) NOT NULL,"
                       + "radnoMesto         VARCHAR(255) NOT NULL,"
                       + "plata              DOUBLE NOT NULL)";

            executeNonQuery(command);

            #endregion

            #region dodatnePogodnosti

            command.CommandText = "CREATE TABLE IF NOT EXISTS dodatnePogodnosti ("
                                 + "idDodatnePogodnosti       INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                                 + "masaza                    BOOLEAN DEFAULT false,"
                                 + "tretmanLica               BOOLEAN DEFAULT false,"
                                 + "sauna                     BOOLEAN DEFAULT false)";
            executeNonQuery(command);

            #endregion 

            #region uslugeAviona

            command.CommandText = "CREATE TABLE IF NOT EXISTS uslugeAviona("
                     + "IDUslugeAviona        INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                     + "IDDodatnePogodnosti   INT NOT NULL,"
                     + "hrana                 VARCHAR(255) NOT NULL,"
                     + "pice                  VARCHAR(255) NOT NULL,"
                     + "FOREIGN KEY(IDDodatnePogodnosti) REFERENCES dodatnePogodnosti(IDDodatnePogodnosti))";
            executeNonQuery(command);

            #endregion

            #region avioni 

            command.CommandText = "CREATE TABLE IF NOT EXISTS avioni ("
                               + "IDAviona            INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                               + "IDUslugeAviona      INT NOT NULL,"
                               + "naziv               VARCHAR(255) NOT NULL,"
                               + "model               DOUBLE NOT NULL,"
                               + "stanje              BOOLEAN DEFAULT true,"
                               + "FOREIGN KEY(IDUslugeAviona) REFERENCES uslugeAviona(IDUslugeAviona))";
            executeNonQuery(command);

            #endregion

            #region letovi 

            command.CommandText = "CREATE TABLE IF NOT EXISTS letovi ("
                                + "IDLeta                INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                                + "IDAviona              INT NOT NULL,"
                                + "otkazivanje           BOOLEAN DEFAULT true,"
                                + "datum                 DATE NOT NULL,"
                                + "pocetnaDestinacija    VARCHAR(255) NOT NULL,"
                                + "krajnjaDestinacija    VARCHAR(255) NOT NULL,"
                                + "FOREIGN KEY(IDAviona) REFERENCES avioni(IDAviona))";
            executeNonQuery(command);

            #endregion 

            #region detaljiLeta

            command.CommandText = "CREATE TABLE IF NOT EXISTS detaljiLeta ("
                                + "IDDetaljiLeta         INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                                + "IDLeta                INT NOT NULL,"
                                + "IDZaposleni           INT NOT NULL,"
                                + "FOREIGN KEY(IDLeta) REFERENCES letovi(IDLeta),"
                                + "FOREIGN KEY(IDZaposleni) REFERENCES zaposleni(IDZaposleni))";
            executeNonQuery(command);

            #endregion

            #region rezervacije

            command.CommandText = "CREATE TABLE IF NOT EXISTS rezervacije ("
                              + "IDRezervacije          INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                              + "IDLeta                 INT NOT NULL,"
                              + "datum                  DATE NOT NULL,"
                              + "vreme                   TIME NOT NULL,"
                              + "FOREIGN KEY(IDLeta) REFERENCES letovi(IDLeta))";
            executeNonQuery(command);

            #endregion 

            #region detaljiRezervacije

            command.CommandText = "CREATE TABLE IF NOT EXISTS detaljiRezervacije("
                          + "IDDetaljaRezervacije         INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                          + "IDPutnika                    INT NOT NULL,"
                          + "IDRezervacije                INT NOT NULL,"
                          + "onlineRezervacija            BOOLEAN DEFAULT true,"
                          + "sedista                      INT NOT NULL,"
                          + "klasa                        VARCHAR(255) NOT NULL,"
                          + "vrstaKarte                   VARCHAR(255) NOT NULL,"
                          + "cenaKarte                    DOUBLE NOT NULL,"
                          + "FOREIGN KEY(IDPutnika)       REFERENCES putnici(IDPutnika),"
                          + "FOREIGN KEY(IDRezervacije)   REFERENCES rezervacije(IDRezervacije))";

            executeNonQuery(command);

            #endregion

        }

        private static void fillTable(MySqlCommand command)
        {
            #region putnici

            string dtforDB = dtfordb(new DateTime(1998, 2, 12));
            string dtforDB1 = dtfordb(new DateTime(1998, 5, 25));
            string dtforDB2 = dtfordb(new DateTime(1998, 7, 17));
            string dtforDB3 = dtfordb(new DateTime(1997, 4, 23));
            string dtforDB4 = dtfordb(new DateTime(1998, 10, 07));
            string dtforDB5 = dtfordb(new DateTime(1996, 1, 20));
            string dtforDB6 = dtfordb(new DateTime(1995, 11, 09));
            string dtforDB7 = dtfordb(new DateTime(1999, 6, 19));
            string dtforDB8 = dtfordb(new DateTime(1998, 3, 4));
            string dtforDB9 = dtfordb(new DateTime(1998, 3, 25));
            string dtforDB10 = dtfordb(new DateTime(1998, 9, 8));
            string dtforDB11 = dtfordb(new DateTime(1997, 4, 12));
            string dtforDB12 = dtfordb(new DateTime(1996, 5, 28));
            string dtforDB13 = dtfordb(new DateTime(1997, 12, 19));
            string dtforDB14 = dtfordb(new DateTime(1998, 1, 15));
            string dtforDB15 = dtfordb(new DateTime(1996, 12, 24));
            string dtforDB16 = dtfordb(new DateTime(1998, 9, 05));
            string dtforDB17 = dtfordb(new DateTime(1997, 3, 28));
            string dtforDB18 = dtfordb(new DateTime(1998, 8, 27));
            string dtforDB19 = dtfordb(new DateTime(1996, 4, 04));

            command.CommandText = "insert INTO putnici VALUES (0, 'Milan', 'Markovic', '" + dtforDB + "', 0631928365, 'milan@gmail.com', 'Mite 23 Kragujevac', 43434, 'Srbija'),"
                + "(0, 'Milica', 'Svrakic', '" + dtforDB1 + "', 0631923834, 'miksi@gmail.com', 'Franje 2 Kula', 656677, 'Hrvatska'),"
                + "(0, 'Goran', 'Ognjenovic', '" + dtforDB2 + "', 0623421365, 'goksi@gmail.com', 'Spaggete li omo 56 Djenova', 42314, 'Italija'),"
                + "(0, 'Ana', 'Draskovic', '" + dtforDB3 + "', 0613456934, 'ana@gmail.com', 'Lepomira Bakica 76 Ljubljana', 7656434, 'Slovenija'),"
                + "(0, 'Stefan', 'Milenkovic', '" + dtforDB4 + "', 0639860834, 'stef@gmail.com', 'Rue de Fille Zeneva', 89789734, 'Svajcarska'),"
                + "(0, 'Gligorije', 'Kovac', '" + dtforDB5 + "', 0631928234, 'gliga@gmail.com', 'Rue de Chanel Nica', 387427834, 'Francuska'),"
                + "(0, 'Suzana', 'Radic', '" + dtforDB6 + "', 0631928309, 'suzana@gmail.com', 'Wien Strasse 78 Ninberg', 143544, 'Nemacka'),"
                + "(0, 'Dragan', 'Milic', '" + dtforDB7 + "', 0631928367, 'dragi@gmail.com', 'Nepoznatog Junaka 76 Elemir', 445345, 'Srbija'),"
                + "(0, 'Dragica', 'Radosavljevic', '" + dtforDB8 + "', 0631928334, 'dragica@gmail.com', 'Backa 75 Donji Milanovac', 64564534, 'Srbija'),"
                + "(0, 'Zeljana', 'Ciric', '" + dtforDB9 + "', 0631934365, 'zeks@gmail.com', 'Mileticeva 98 Novi Sad', 54357, 'Srbija'),"
                + "(0, 'Zeljana', 'Ciric', '" + dtforDB10 + "', 0631934365, 'zeks@gmail.com', 'Mileticeva 98 Novi Sad', 54357, 'Srbija'),"
                + "(0, 'Nikola', 'Vulovic', '" + dtforDB11 + "', 0631928565, 'nidza@gmail.com', 'Cara Lazara 87 Pancevo', 5496940, 'Srbija'),"
                + "(0, 'Vuk', 'Mrkonjic', '" + dtforDB12 + "', 0631924355, 'vuk@gmail.com', 'Rumenacka 65 Idvor', 575675, 'Srbija'),"
                + "(0, 'Boban', 'Miric', '" + dtforDB13 + "', 0631929875, 'boki@gmail.com', 'Dunavska 98 Zrenjanin', 65465434, 'Srbija'),"
                + "(0, 'Mitar', 'Vasiljevic', '" + dtforDB14 + "', 0644328365, 'mitar@gmail.com', 'Bajina 67 Kraljevo', 76597934, 'Srbija'),"
                + "(0, 'Darko', 'Savic', '" + dtforDB15 + "', 0609728365, 'dare@gmail.com', 'Sremacka 54 Nis', 645645, 'Srbija'),"
                + "(0, 'Petar', 'Kretler', '" + dtforDB16 + "', 0669828365, 'petar@gmail.com', 'Miodraga Koraca 23 Belo Polje', 65454, 'Srbija'),"
                + "(0, 'Radivoje', 'Djurovic', '" + dtforDB17 + "', 0657828365, 'radivoje@gmail.com', 'Augusta Cesarca 87 Gornji Milanovac', 76534, 'Srbija'),"
                + "(0, 'Anastasija', 'Lazic', '" + dtforDB18 + "', 0623928365, 'anastasija@gmail.com', 'Dalmatinska 12 Dragusica', 945896589, 'Srbija'),"
                + "(0, 'Lenka', 'Petrovic', '" + dtforDB19 + "', 0634568365, 'lenka@gmail.com', 'Vuka Draskovica 35 Knin', 989457959, 'Srbija');";
            executeNonQuery(command);

            #endregion

            #region zaposleni

            command.CommandText = "insert INTO zaposleni VALUES (NULL, 'Marko', 'Markovic', 'pilot', 4000),"
               + "(NULL, 'Milos', 'Radovic', 'pilot', 4500),"
               + "(NULL, 'Aleksa', 'Aleksic', 'pilot', 5000),"
               + "(NULL, 'Ilija', 'Perovic', 'pilot', 5500),"
               + "(NULL, 'Viktor', 'Vulevic', 'pilot', 4500),"
               + "(NULL, 'Ana', 'Stanojevic', 'stjuardesa', 2000),"
               + "(NULL, 'Aleksandra', 'Dragojevic', 'stjuardesa', 2000),"
               + "(NULL, 'Marijana', 'Tomasevic', 'stjuardesa', 2000),"
               + "(NULL, 'Nastasija', 'Dincic', 'stjuardesa', 2000),"
               + "(NULL, 'Leonora', 'Damjanovic', 'stjuardesa', 2000),"
               + "(NULL, 'Tijana', 'Tinic', 'fizioterapeut', 1500),"
               + "(NULL, 'Tamara', 'Radakovic', 'kozmeticar',1000),"
               + "(NULL, 'Milos', 'Misic', 'fizioterapeut', 1500),"
               + "(NULL, 'Katarina', 'Vucenovic', 'fizioterapeut', 1500),"
               + "(NULL, 'Marko', 'Nikolovkski', 'kozmeticar', 1000),"
               + "(NULL, 'Filip', 'Ostojic', 'kozmeticar', 1000),"
               + "(NULL, 'Ognjen', 'Maksimovic', 'pilot', 4500),"
               + "(NULL, 'Aleksandar', 'Filipovic', 'kozmeticar', 1000),"
               + "(NULL, 'Ana', 'Sidovski','kozmeticar', 1000),"
               + "(NULL, 'Helena', 'Sladojevic', 'stjuardesa', 2000);";
            executeNonQuery(command);
            #endregion

            #region dodatnePogodnosti 

            command.CommandText = "insert INTO dodatnePogodnosti VALUES (NULL, false, false, false),"
               + "(NULL, false, true, false),"
               + "(NULL, false, true, false),"
               + "(NULL, false, false, true),"
               + "(NULL, false, false, true),"
               + "(NULL, true, false, false),"
               + "(NULL, true, false, false),"
               + "(NULL, false, true, true),"
               + "(NULL, false, false, true),"
               + "(NULL, false, true, false),"
               + "(NULL, false, true, false),"
               + "(NULL, true, true, true),"
               + "(NULL, true, false, false),"
               + "(NULL, true, false, false),"
               + "(NULL, false, true, true),"
               + "(NULL, false, false, true),"
               + "(NULL, true, true, true),"
               + "(NULL, true, true, false),"
               + "(NULL, false, true, true),"
               + "(NULL, false, true, true);";


            executeNonQuery(command);
            #endregion

            #region uslugeAviona

            command.CommandText = "insert into uslugeAviona values(0, 7, 'pica', 'koka kola'),"
                        + "(0, 10, 'tortija', 'fanta'),"
                        + "(0, 13, 'hamburger', 'sprajt'),"
                        + "(0, 15, 'ciburger', 'limunada'),"
                        + "(0, 5, 'pomfrit', 'voda'),"
                        + "(0, 19, 'hot dog', 'sveps'),"
                        + "(0, 14, 'cevapi', 'sok od visnje'),"
                        + "(0, 12, 'burito', 'sok od narandze'),"
                        + "(0, 6, 'pljeskavica', 'sok od jabuke'),"
                        + "(0, 18, 'cezar salata', 'guarana'),"
                        + "(0, 20, 'lazanje', 'red bull'),"
                        + "(0, 11, 'pohovana piletina', 'kokta'),"
                        + "(0, 9, 'bolonjeze', 'sok od borovnice'),"
                        + "(0, 1, 'karbonara', 'sok od breskve'),"
                        + "(0, 2, 'palacinke', 'sok od kupine'),"
                        + "(0, 3, 'sladoled', 'sok od jagode'),"
                        + "(0, 4, 'pohovani kackavalj', 'seven up'),"
                        + "(0, 8, 'riba', 'pepsi'),"
                        + "(0, 1, 'grilovano povrce', 'gazirana voda'),"
                        + "(0, 5, 'rizoto sa pecurkama', 'sok od dunje')";
            executeNonQuery(command);
            #endregion

            #region avioni 

            command.CommandText = "insert INTO avioni VALUES (NULL, 10, 'luksuzni', 'Boing 747', 'ispravan'),"
               + "(NULL, 11, 'luksuzni', 'Airbus A800', 'ispravan'),"
               + "(NULL, 9, 'luksuzni', 'Boing 750', 'ispravan'),"
               + "(NULL, 12, 'luksuzni', 'Boing 737', 'ispravan'),"
               + "(NULL, 14, 'luksuzni', 'Airbus A300', 'ispravan'),"
               + "(NULL, 15, 'luksuzni', 'Airbus A550', 'ispravan'),"
               + "(NULL, 13, 'luksuzni', 'Boing 733', 'ispravan'),"
               + "(NULL, 6, 'putnicki', 'Concord FJet', 'ispravan'),"
               + "(NULL, 5, 'putnicki', 'Concord B700', 'ispravan'),"
               + "(NULL, 4, 'putnicki', 'Dassault Falcon', 'ispravan'),"
               + "(NULL, 3, 'putnicki', 'Dassault 20', 'ispravan'),"
               + "(NULL, 2, 'putnicki', 'Dassault 50', 'ispravan'),"
               + "(NULL, 1, 'putnicki', 'Gulfstream Tomcat', 'ispravan'),"
               + "(NULL, 20, 'luksuzni', 'Cessna Piper', 'ispravan'),"
               + "(NULL, 19, 'luksuzni', 'Cessna Textron', 'ispravan'),"
               + "(NULL, 18, 'putnicki', 'Concord Jet', 'neispravan'),"
               + "(NULL, 17, 'luksuzni', 'Boing 837', 'neispravan'),"
               + "(NULL, 16, 'luksuzni', 'Airbus B250', 'ispravan'),"
               + "(NULL, 12, 'putnicki', 'Pilatus BIT77', 'ispravan'),"
               + "(NULL, 3, 'putnicki', 'Pilatus pc12', 'ispravan');";
            executeNonQuery(command);
            #endregion

            #region letovi

            command.CommandText = "insert INTO letovi VALUES (NULL, 1, false, '2019-10-1', 'Srbija', 'Tokio'),"
               + "(NULL, 2, true,  '2019-10-3', 'Srbija', 'Francuska'),"
               + "(NULL, 5, true,  '2016-10-1', 'Srbija', 'Nemacka'),"
               + "(NULL, 4, false, '2016-10-09', 'Srbija', 'Tokio'),"
               + "(NULL, 10, true,  '2019-10-05', 'Srbija', 'Nemacka'),"
               + "(NULL, 11, true , '2019-07-13', 'Srbija', 'Finska'),"
               + "(NULL, 14, false, '2019-05-11', 'Srbija', 'Danska'),"
               + "(NULL, 13, true,  '2016-10-18', 'Srbija', 'Kina'),"
               + "(NULL, 7, false, '2019-08-13', 'Srbija', 'Kina'),"
               + "(NULL, 8, true,  '2019-10-03', 'Srbija', 'Amerika'),"
               + "(NULL, 9, false, '2019-05-13', 'Srbija', 'Amerika'),"
               + "(NULL, 16, true,  '2019-10-03', 'Srbija', 'Nemacka'),"
               + "(NULL, 19, false, '2019-10-06', 'Srbija', 'Finska'),"
               + "(NULL, 20, true,  '2016-10-03', 'Srbija', 'Danska'),"
               + "(NULL, 3, true,  '2019-07-13', 'Srbija', 'Monako'),"
               + "(NULL, 6, true,  '2016-06-13', 'Srbija', 'Nemacka'),"
               + "(NULL, 14, true,  '2019-10-22', 'Srbija', 'Monako'),"
               + "(NULL, 15, false, '2016-10-17', 'Srbija', 'Kuba'),"
               + "(NULL, 18, true,  '2019-10-13', 'Srbija', 'Monako'),"
               + "(NULL, 13, true,  '2016-10-17', 'Srbija', 'Kina');";
            executeNonQuery(command);
            #endregion 

            #region detaljiLeta

            command.CommandText = "insert INTO detaljiLeta VALUES (NULL, 1, 3),"
               + "(NULL, 2, 8),"
               + "(NULL, 14, 17),"
               + "(NULL, 15, 10),"
               + "(NULL, 19, 6),"
               + "(NULL, 2, 19),"
               + "(NULL, 20, 14),"
               + "(NULL, 15, 7),"
               + "(NULL, 19, 17),"
               + "(NULL, 18, 9),"
               + "(NULL, 12, 4),"
               + "(NULL, 13, 10),"
               + "(NULL, 11, 8),"
               + "(NULL, 16, 3),"
               + "(NULL, 15, 10),"
               + "(NULL, 18, 5),"
               + "(NULL, 7, 4),"
               + "(NULL, 12, 13),"
               + "(NULL, 10, 2),"
               + "(NULL, 16, 18);";
            executeNonQuery(command);
            #endregion

            #region rezervacije

            command.CommandText = "insert INTO rezervacije VALUES (NULL, 1, '2018-01-15', '05:40:00'),"
               + "(NULL, 3, '2017-01-22', '07:15:00'),"
               + "(NULL, 4, '2018-02-16', '11:00:00'),"
               + "(NULL, 7, '2017-03-22', '12:15:00'),"
               + "(NULL, 8, '2016-04-27', '17:00:00'),"
               + "(NULL, 9, '2016-03-22', '14:30:00'),"
               + "(NULL, 10, '2016-06-18', '17:45:00'),"
               + "(NULL, 15, '2018-05-16', '12:00:00'),"
               + "(NULL, 14, '2018-08-07', '14:15:00'),"
               + "(NULL, 20, '2017-12-22', '15:00:00'),"
               + "(NULL, 13, '2015-07-27', '13:00:00'),"
               + "(NULL, 11, '2015-04-17', '08:00:00'),"
               + "(NULL, 7, '2015-10-19', '10:00:00'),"
               + "(NULL, 6, '2016-11-03', '11:15:00'),"
               + "(NULL, 5, '2017-05-26', '16:45:00'),"
               + "(NULL, 17, '2019-03-04', '12:30:00'),"
               + "(NULL, 18, '2019-02-27', '13:30:00'),"
               + "(NULL, 19, '2019-04-15', '22:00:00'),"
               + "(NULL, 13, '2019-03-18', '21:00:00'),"
               + "(NULL, 7, '2010-02-13', '17:15:00');";
            executeNonQuery(command);
            #endregion 

            #region detaljiRezervacije

            command.CommandText = "insert INTO detaljiRezervacije VALUES (NULL, 1, 4, 'false', 1, 'I', 'early', 800),"
               + "(NULL, 5, 8, 'false', 2, 'I', 'early', 800),"
               + "(NULL, 7, 4, 'true', 3, 'I', 'early', 800),"
               + "(NULL, 10, 15, 'true', 4, 'II', 'late flight', 1000),"
               + "(NULL, 18, 19, 'true', 5, 'I', 'early', 800),"
               + "(NULL, 20, 9, 'false', 6, 'I', 'late flight', 1000),"
               + "(NULL, 1, 3, 'false', 7, 'I', 'late flight', 800),"
               + "(NULL, 5, 7, 'true', 8, 'II', 'early', 1000),"
               + "(NULL, 10, 17, 'false' , 9, 'I', 'late flight', 800),"
               + "(NULL, 14, 12, 'true', 10, 'I', 'early', 1000),"
               + "(NULL, 18, 19, 'true', 11, 'II', 'late flight', 1000),"
               + "(NULL, 11, 20, 'false', 12, 'I', 'late flight', 1000),"
               + "(NULL, 7, 10, 'true', 13, 'I', 'ealry', 800),"
               + "(NULL, 5, 9, 'true', 14, 'I', 'early', 800),"
               + "(NULL, 9, 4, 'false', 15, 'I', 'late flight', 800),"
               + "(NULL, 12, 19, 'true', 16, 'II', 'early', 1000),"
               + "(NULL, 17, 10, 'false', 17, 'I', 'early', 800),"
               + "(NULL, 9, 12, 'true', 18, 'I', 'early', 800),"
               + "(NULL, 16, 8, 'false', 19, 'I', 'late flight', 800),"
               + "(NULL, 17, 9, 'true', 20, 'II', 'early', 1000);";
            executeNonQuery(command);
            #endregion 

        }

        private static void selektovi(MySqlCommand command)
        {


            Console.WriteLine("Dobrodosli u aviokompaniju FLY HIGH");
            Console.WriteLine("");

            command.CommandText = "SELECT Letovi_2019,Letovi_2016" +
                                  " FROM " +
                                  "(SELECT COUNT(x.IDLeta) Letovi_2019 FROM letovi x WHERE x.datum BETWEEN '2019-01-01' AND '2019-12-31') A, " +
                                  "(SELECT COUNT(y.IDLeta) Letovi_2016 FROM letovi y WHERE y.datum BETWEEN '2016-01-01' AND '2016-12-31')B";


            System.Data.DataSet ubb1 = executeSelect(command);


            Console.WriteLine("1. Letovi 2019, 2018: "); 
            for (int podaci = 0; podaci < ubb1.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb1.Tables[0].Rows[podaci]["Letovi_2019"] + " \t" + ubb1.Tables[0].Rows[podaci]["Letovi_2016"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT 0.50 * (SELECT SUM(cenaKarte) from detaljirezervacije dr " +
                                    "INNER JOIN rezervacije r ON r.IDRezervacije = dr.IDRezervacije " +
                                    "INNER JOIN letovi l ON l.IDLeta = r.IDLeta " +
                                    "WHERE l.otkazivanje = 1) as cena_karte";

            System.Data.DataSet ubb2 = executeSelect(command);

            Console.WriteLine(ubb2);

            Console.WriteLine("2. Troskovi su : ");
            for (int podaci = 0; podaci < ubb2.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb2.Tables[0].Rows[podaci]["cena_karte"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT (COUNT(*)*100/c.ukupan_broj_putnika) as procenat_putnika FROM rezervacije," +
                "(SELECT letovi.IDLeta as let FROM letovi," +
                "(SELECT avioni.IDAviona as avion FROM avioni WHERE avioni.naziv='luksuzni')a " +
                "WHERE letovi.IDAviona = a.avion)b," +
                "(SELECT COUNT(*) as ukupan_broj_putnika FROM rezervacije)c " +
                "WHERE rezervacije.IDLeta=b.let";

            System.Data.DataSet ubb3 = executeSelect(command);

            Console.WriteLine("3. Procenat putnika koji je putovao lux avionima je : ");
            for (int podaci = 0; podaci < ubb3.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb3.Tables[0].Rows[podaci]["procenat_putnika"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT COUNT(*) as broj_vaucera " +
                   "FROM rezervacije " +
                                   "WHERE MONTH(rezervacije.datum) = 12 OR MONTH(rezervacije.datum)=1 ";

            System.Data.DataSet ubb4 = executeSelect(command);

            Console.WriteLine("4. Vaucera se iskoristi : ");
            for (int podaci = 0; podaci < ubb4.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb4.Tables[0].Rows[podaci]["broj_vaucera"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT p.ime, p.prezime, dr.onlineRezervacija, dr.klasa, dr.vrstaKarte, dr.cenaKarte, r.datum " +
                   "FROM detaljirezervacije dr " +
                                   "INNER JOIN rezervacije r " +
                               "ON r.IDRezervacije = dr.IDRezervacije " +
                                   "INNER JOIN putnici p " +
                               "ON p.idPutnika = dr.IDPutnika " +
                                   "INNER JOIN letovi l " +
                              "ON l.IDLeta = r.IDLeta " +
                                  "WHERE l.otkazivanje = 1";

            System.Data.DataSet ubb5 = executeSelect(command);

            Console.WriteLine("5. Otkazani letovi : ");
            for (int podaci = 0; podaci < ubb5.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb5.Tables[0].Rows[podaci]["ime"] + " | " + ubb5.Tables[0].Rows[podaci]["prezime"] + " | " + ubb5.Tables[0].Rows[podaci]["onlineRezervacija"] + " | " + ubb5.Tables[0].Rows[podaci]["klasa"] + " | " + ubb5.Tables[0].Rows[podaci]["vrstaKarte"] + " | " + ubb5.Tables[0].Rows[podaci]["CenaKarte"] + " | " + ubb5.Tables[0].Rows[podaci]["datum"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT a.early, b.last_minute " +
                   "FROM (SELECT COUNT(*) as early " +
                       "FROM detaljirezervacije " +
                       "INNER JOIN rezervacije ON rezervacije.IDRezervacije = detaljirezervacije.IDRezervacije " +
                "WHERE detaljirezervacije.vrstaKarte='early' AND YEAR(rezervacije.datum)=2018 AND MONTH(rezervacije.datum) = 8)a, " +
                "(SELECT COUNT(*) as last_minute " +
                "FROM detaljirezervacije " +
                "INNER JOIN rezervacije ON rezervacije.IDRezervacije = detaljirezervacije.IDRezervacije " +
                "WHERE detaljirezervacije.vrstaKarte='last-minute' AND YEAR(rezervacije.datum)=2018 AND MONTH(rezervacije.datum) = 8)b";

            System.Data.DataSet ubb6 = executeSelect(command);

            Console.WriteLine("6. Prodatih early flight karata i last-minut flight karata za sve letove u toku avgusta 2018 godine je:  ");
            for (int podaci = 0; podaci < ubb6.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb6.Tables[0].Rows[podaci]["early"] + "\t " + ubb6.Tables[0].Rows[podaci]["last_minute"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT A.Letovi_2019, B.Letovi_2016 " +
                   "FROM " +
                       "(SELECT COUNT(x.IDLeta) Letovi_2019 FROM letovi x WHERE x.datum BETWEEN '2019-01-01' AND '2019-12-31') A, " +
                                   "(SELECT COUNT(y.IDLeta) Letovi_2016 FROM letovi y WHERE y.datum BETWEEN '2016-01-01' AND '2016-12-31')B";

            System.Data.DataSet ubb7 = executeSelect(command);

            Console.WriteLine("7. Porast letova je: ");
            for (int podaci = 0; podaci < ubb7.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb7.Tables[0].Rows[podaci]["Letovi_2019"] + " \t " + ubb7.Tables[0].Rows[podaci]["Letovi_2016"]); ///
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT c.avion, avioni.IDUslugeAviona, uslugeaviona.hrana, uslugeaviona.pice " +
                   "FROM avioni " +
                   "INNER JOIN uslugeaviona ON uslugeaviona.IDUslugeAviona = avioni.IDUslugeAviona, " +
                   "(SELECT letovi.IDAviona as avion " +
                   "FROM letovi, " +
                                   "(SELECT rezervacije.IDLeta as let " +
                                   "FROM rezervacije, " +
                                   "(SELECT detaljirezervacije.IDRezervacije as rezervacija " +
                                   "FROM detaljirezervacije " +
                                   "WHERE detaljirezervacije.klasa = 'I')a " +
                                   "WHERE rezervacije.IDRezervacije = a.rezervacija) b " +
                                   "WHERE letovi.IDLeta = b.let)c " +
                                   "WHERE avioni.IDAviona = c.avion AND avioni.naziv = 'putnicki'";

            System.Data.DataSet ubb8 = executeSelect(command);

            Console.WriteLine("8. U putnickoj klasi se prosecno proda hrane i pica: ");
            for (int podaci = 0; podaci < ubb8.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb8.Tables[0].Rows[podaci]["avion"] + " |  " + ubb8.Tables[0].Rows[podaci]["IDUslugeAviona"] + " |  " + ubb8.Tables[0].Rows[podaci]["hrana"] + " |  " + ubb8.Tables[0].Rows[podaci]["pice"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT dodatnepogodnosti.masaza as masaza, dodatnepogodnosti.tretmanLica as tretmanLica, dodatnepogodnosti.sauna as sauna " +
                   "FROM uslugeaviona " +
                   "INNER JOIN dodatnepogodnosti ON dodatnepogodnosti.idDodatnePogodnosti = uslugeaviona.IDDodatnePogodnosti, " +
                   "(SELECT avioni.IDUslugeAviona as usluge " +
                   "FROM avioni " +
                   "INNER JOIN letovi ON letovi.IDAviona=avioni.IDAviona " +
                   "WHERE avioni.naziv = 'luksuzni')a " +
                   "WHERE uslugeaviona.IDUslugeAviona = a.usluge";

            System.Data.DataSet ubb9 = executeSelect(command);

            Console.WriteLine("9. Broj putnika koji koristi lux pakete: ");
            for (int podaci = 0; podaci < ubb9.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb9.Tables[0].Rows[podaci]["masaza"] + " | " +
                    ubb9.Tables[0].Rows[podaci]["tretmanLica"] + " | " +
                    ubb9.Tables[0].Rows[podaci]["sauna"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT a.IDAviona, count(r.IDRezervacije) as BrojRezervacija " +
                       "FROM rezervacije r " +
                       "INNER JOIN letovi l " +
                   "ON l.IDLeta = r.IDLeta " +
                   "INNER JOIN avioni a " +
                          "ON a.IDAviona = l.IDAviona " +
                   "GROUP BY a.IDAviona " +
                   "ORDER BY BrojRezervacija desc " +
                   "LIMIT 0,1";

            System.Data.DataSet ubb10 = executeSelect(command);

            Console.WriteLine("10. Prikazujemo avion koji je imao najveci broj rezervacija u toku godine:  ");
            for (int podaci = 0; podaci < ubb10.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb10.Tables[0].Rows[podaci]["BrojRezervacija"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "select count(dp.masaza) as Masaza " +
                           "FROM detaljirezervacije dr " +

                   "INNER JOIN rezervacije r " +
                   "ON r.IDRezervacije = dr.IDRezervacije " +
                       "INNER JOIN letovi l " +
                   "ON l.IDLeta = r.IDLeta " +
                   "INNER JOIN avioni a " +
                   "ON a.IDAviona = l.IDAviona " +
                   "INNER JOIN uslugeaviona ua " +
                       "ON ua.IDUslugeAviona = a.IDUslugeAviona " +
                   "INNER JOIN dodatnepogodnosti dp " +
                   "ON dp.idDodatnePogodnosti = ua.IDDodatnePogodnosti " +
                   "WHERE dp.masaza = 1 AND dr.vrstaKarte = 'early'";
            System.Data.DataSet ubb12 = executeSelect(command);

            Console.WriteLine("12. Putnici koji koriste dodatnu pogodnost “Masaža” u luksuznim “early” letovima : ");
            for (int podaci = 0; podaci < ubb12.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb12.Tables[0].Rows[podaci]["masaza"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            
            command.CommandText = "SELECT zaposleni.ime, zaposleni.prezime, " +
                                    "COUNT(*) as ukupan_broj_letova " +
                                    "FROM detaljileta "+
                                    "INNER JOIN zaposleni ON zaposleni.idZaposleni=detaljileta.IDZaposleni, " +
                                    "(SELECT zaposleni.idZaposleni as zaposleni "+
                                    "FROM zaposleni "+
                                    "WHERE zaposleni.radnoMesto='pilot')a "+
                                    "WHERE detaljileta.IDZaposleni = a.zaposleni "+
                                    "GROUP BY detaljileta.IDZaposleni "+
                                    "ORDER BY ukupan_broj_letova DESC";

            System.Data.DataSet ubb13 = executeSelect(command);

            Console.WriteLine("13. Ime i prezime putnika koji je ucestvovao u najvise letova : ");
            for (int podaci = 0; podaci < ubb13.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb13.Tables[0].Rows[podaci]["ime"] + " |  " + ubb13.Tables[0].Rows[podaci]["prezime"] + " |  " + ubb13.Tables[0].Rows[podaci]["ukupan_broj_letova"]);
            }
            Console.WriteLine(""); 
            

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT (COUNT(*)*100/c.ukupan_broj_putnika) as procenat_putnika " +
                   "FROM rezervacije,(SELECT letovi.IDLeta as let " +
                   "FROM letovi,(SELECT avioni.IDAviona as avion " +
                   "FROM avioni " +
                   "WHERE avioni.naziv='putnicki')a " +
                   "WHERE letovi.IDAviona = a.avion)b,(SELECT COUNT(*) as ukupan_broj_putnika " +
                   "FROM rezervacije)c " +
                   "WHERE rezervacije.IDLeta=b.let";

            System.Data.DataSet ubb14 = executeSelect(command);

            Console.WriteLine("14. Procenat putnika koji je putovao obicnim avionom: ");
            for (int podaci = 0; podaci < ubb14.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb14.Tables[0].Rows[podaci]["procenat_putnika"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT COUNT(*) as broj_otkazanih_letova_2016_2017 " +
                   "FROM letovi " +
                   "WHERE letovi.otkazivanje = 1 AND YEAR(letovi.datum) > 2015 AND YEAR(letovi.datum) < 2018";

            System.Data.DataSet ubb15 = executeSelect(command);

            Console.WriteLine("15. Broj otkazanih letova u periodu od 2016 do 2017 u odnosu na prethodnu godinu: ");
            for (int podaci = 0; podaci < ubb15.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb15.Tables[0].Rows[podaci]["broj_otkazanih_letova_2016_2017"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT * FROM detaljirezervacije " +
                       "WHERE cenaKarte = (SELECT MAX(cenaKarte) FROM detaljirezervacije)";

            System.Data.DataSet ubb16 = executeSelect(command);

            Console.WriteLine("16. Prikaz prodatih najskupljih karata je :  ");
            for (int podaci = 0; podaci < ubb16.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb16.Tables[0].Rows[podaci]["IDDetaljaRezervacije"] + " | " +
                    ubb16.Tables[0].Rows[podaci]["IDPutnika"] + " | " +
                    ubb16.Tables[0].Rows[podaci]["IDRezervacije"] + " | " +
                    ubb16.Tables[0].Rows[podaci]["onlineRezervacija"] + " | " +
                    ubb16.Tables[0].Rows[podaci]["sedista"] + " | " +
                    ubb16.Tables[0].Rows[podaci]["klasa"] + " | " +
                    ubb16.Tables[0].Rows[podaci]["vrstaKarte"] + " | " +
                    ubb16.Tables[0].Rows[podaci]["cenaKarte"] + " | ");
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");


            command.CommandText = "select (SELECT SUM(dr.cenaKarte) FROM rezervacije r " +
                   "INNER JOIN detaljirezervacije dr " +
                   "ON dr.IDRezervacije = r.IDRezervacije " +
                   "WHERE YEAR(r.datum) = 2010) as '2010', " +
                   "(SELECT SUM(dr.cenaKarte) FROM rezervacije r " +
                   "INNER JOIN detaljirezervacije dr " +
                   "ON dr.IDRezervacije = r.IDRezervacije " +
                   "WHERE YEAR(r.datum) = 2015) as '2015', " +
                   "(SELECT SUM(dr.cenaKarte) FROM rezervacije r " +
                   "INNER JOIN detaljirezervacije dr " +
                           "ON dr.IDRezervacije = r.IDRezervacije " +
                           "WHERE YEAR(r.datum) = 2016) as '2016', " +
                           "(SELECT SUM(dr.cenaKarte) FROM rezervacije r " +
                       "INNER JOIN detaljirezervacije dr " +
                      "ON dr.IDRezervacije = r.IDRezervacije " +
                   "WHERE YEAR(r.datum) = 2017) as '2017', " +
                   "(SELECT SUM(dr.cenaKarte) FROM rezervacije r " +
                       "INNER JOIN detaljirezervacije dr " +
                   "ON dr.IDRezervacije = r.IDRezervacije " +
                           "WHERE YEAR(r.datum) = 2018) as '2018', " +
                       "(SELECT SUM(dr.cenaKarte) FROM rezervacije r " +
                   "INNER JOIN detaljirezervacije dr " +
                       "ON dr.IDRezervacije = r.IDRezervacije " +
                    "WHERE YEAR(r.datum) = 2019) as '2019'";
            System.Data.DataSet ubb17 = executeSelect(command);

            Console.WriteLine("17. Zarada po godinama je : ");
            for (int podaci = 0; podaci < ubb17.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
               
                Console.WriteLine("2010: " + ubb17.Tables[0].Rows[podaci]["2010"] + "2015: " + ubb17.Tables[0].Rows[podaci]["2015"] + "2016: " + ubb17.Tables[0].Rows[podaci]["2016"] + "2017: " + ubb17.Tables[0].Rows[podaci]["2017"] + "2018: " + ubb17.Tables[0].Rows[podaci]["2018"] + "2019: " + ubb17.Tables[0].Rows[podaci]["2019"]);
                
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT (SELECT count(p.idPutnika) FROM putnici p " +
                   "INNER JOIN detaljirezervacije dr " +
                   "ON dr.IDPutnika = p.idPutnika " +
                   "INNER JOIN rezervacije r " +
                   "ON r.IDRezervacije = dr.IDRezervacije " +
                   "WHERE klasa = \"I klasa\" AND YEAR(r.datum) = 2019) - (SELECT count(p.idPutnika) FROM putnici p " +
                   "INNER JOIN detaljirezervacije dr " +
                   "ON dr.IDPutnika = p.idPutnika " +
                   "INNER JOIN rezervacije r " +
                   "ON r.IDRezervacije = dr.IDRezervacije " +
                       "WHERE klasa = \"I klasa\" AND YEAR(r.datum) = 2018) AS broj_putnika";

            System.Data.DataSet ubb18 = executeSelect(command);

            Console.WriteLine("18. Putnici koji su koristili putnicku klasu u 2019 godini u odnosu na prethodnu : ");
            for (int podaci = 0; podaci < ubb18.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb18.Tables[0].Rows[podaci]["broj_putnika"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "select x.IDLeta, x.pocetnaDestinacija, x.krajnjaDestinacija, COUNT(*) " +
                   "from letovi as x " +
                           "inner join rezervacije as re on x.IDLeta=re.IDRezervacije " +
                       "GROUP BY x.krajnjaDestinacija " +
                   "ORDER by COUNT(*) DESC " +
                   "LIMIT 1";
            System.Data.DataSet ubb19 = executeSelect(command);

            Console.WriteLine("19. Najcesca destinacija je: ");
            for (int podaci = 0; podaci < ubb19.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                
                Console.WriteLine(ubb19.Tables[0].Rows[podaci]["krajnjaDestinacija"]); ///
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT a.IDAviona, count(r.IDRezervacije) as BrojRezervacija " +
                   "FROM rezervacije r " +
                       "INNER JOIN letovi l " +
                   "ON l.IDLeta = r.IDLeta " +
                   "INNER JOIN avioni a " +
                       "ON a.IDAviona = l.IDAviona " +
                   "GROUP BY a.IDAviona " +
                       "ORDER BY BrojRezervacija desc " +
                   "LIMIT 0,1";
            System.Data.DataSet ubb20 = executeSelect(command);

            Console.WriteLine("20. Avion koji je imao najveci broj rezervacija u toku godine:  ");
            for (int podaci = 0; podaci < ubb20.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb20.Tables[0].Rows[podaci]["BrojRezervacija"]);
            }
            Console.WriteLine("");

            Console.WriteLine("-----------------------------------------------------------------");

            command.CommandText = "SELECT ((SELECT SUM(dr.cenaKarte) FROM detaljirezervacije dr " +
                   "INNER JOIN rezervacije r " +
                   "ON r.IDRezervacije = dr.IDRezervacije " +
                  "WHERE YEAR(r.datum) = 2019) - (SELECT 12 * SUM(plata) FROM zaposleni)) as GodisnjiBilans";

            System.Data.DataSet ubb21 = executeSelect(command);

            Console.WriteLine("21. Bilans uspeha je : ");
            for (int podaci = 0; podaci < ubb21.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb21.Tables[0].Rows[podaci]["GodisnjiBilans"]);
            }
        }

        public static string dtfordb(DateTime dt)
        {
            return dt.Year + "-" + dt.Month + "-" + dt.Day + " " + dt.TimeOfDay;
        }

        public static void executeNonQuery(MySqlCommand commandsw)
        {
            bool b = false;

            while (!b)
            {
                try
                {
                    if (!commandsw.Connection.Ping() || commandsw.Connection.State == System.Data.ConnectionState.Closed)
                    {
                        commandsw.Connection.Open();
                    }
                    commandsw.ExecuteNonQuery();
                    b = true;
                }
                catch
                {
                    Console.WriteLine(DateTime.Now + " Vrtimo se u petlji upisa u bazu");
                    Console.WriteLine(DateTime.Now + " " + commandsw.CommandText);
                    b = false;
                }
            }
        } 

        public static System.Data.DataSet executeSelect(MySqlCommand command)
        {
            bool b = false;
            System.Data.DataSet cas = new System.Data.DataSet();

            while (!b)
            {
                try
                {
                    if (!command.Connection.Ping() || command.Connection.State == System.Data.ConnectionState.Closed)
                    {
                        command.Connection.Open();
                    }
                    MySqlDataAdapter adapter = new MySqlDataAdapter(command.CommandText, command.Connection);
                    adapter.Fill(cas);
                    b = true;
                }
                catch
                {
                    Console.WriteLine(DateTime.Now + "Vrtimo se u petlji SELECT u bazi");
                    Console.WriteLine(DateTime.Now + " " + command.CommandText);
                    b = false;
                }
            }
            return cas;
        }

    }
}
